<?php

/**
 * @return array|mixed
 */
function generic_colour_regenerate(){
  return drupal_get_form('generic_colour_regenerateForm');
}

/**
 * @param $form_state
 * @return array
 */
function generic_colour_regenerateForm($form_state){
  $form = array();
  $form['generic_colour_regmarkup'] = array(
    '#markup' => '<h2>Regenerate colour index</h2><p><strong>NOTE: </strong> this will delete all existing colour data previously generated. <br/>It will not delete any images, just the nodes of type Generic Colours. It will also take a very long time to run depending on the amount of images available.</p>'
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Delete existing colour index and regenerate',
  );
  $form['#submit'][] = 'generic_colour_regenerateForm_submit';

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function generic_colour_regenerateForm_submit($form, &$form_state){
  // Delete all existing nodes
  $node_type = 'generic_colours';
  $sql = "SELECT nid FROM {node} n WHERE n.type = :type";
  $result = db_query($sql, array(':type' => $node_type));
  $nodeids = array();
  foreach ($result as $row) {
    $nodeids[] = $row->nid;
  }
  node_delete_multiple($nodeids);
  drupal_set_message("Deleted " . count($nodeids) . " nodes.");

  // Iterate through all managed files, find images and index colours
  $results = db_query("SELECT fm.fid as fid FROM {file_managed} fm JOIN {file_usage} fu ON fm.fid = fu.fid WHERE filemime like 'image%' AND type <> 'user' ORDER BY fu.fid asc");
  $allfileids = array();
  foreach($results as $v){
    $allfileids[] = $v->fid;
  }

  if($results->rowCount() >0){
    $batch = array(
      'operations' => array(
        array('generic_colour_batch_process', array($allfileids))
      ),
      'finished' => 'generic_colour_batch_finished',
      'title' => t('Regenerating image colours'),
      'init_message' => t('Regenerating image colours is starting.'),
      'progress_message' => t('Processing batch...'),
      'error_message' => t('Regenerating image colours Batch has encountered an error.'),
    );
    batch_set($batch);
  }
}

/**
 * @param $itemu
 * @param $context
 */
function generic_colour_batch_process($allfileids, &$context){
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = count($allfileids);
  }
  // With each pass through the callback, retrieve the next group of nids
  $result = db_query_range("SELECT fm.fid FROM {file_managed} fm JOIN {file_usage} fu ON fm.fid = fu.fid WHERE fm.fid > ". $context['sandbox']['current_node'] ." AND filemime like 'image%' AND type <> 'user' ORDER BY fid ASC", 0, 1);
  foreach($result as $row){
    // Here we actually perform our processing on the current file.
    $file = file_load($row->fid);
    // Adding the source as it's needed for the filters in the next function
    $file->source = "_generic_colours_";
    // Processing images
    generic_colours_analyze_insert($file);
    // Store some result for post-processing in the finished callback.
    $context['results'][] = check_plain($file->filename); //file name
    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $file->fid;
    $context['message'] = t('Now processing %node', array('%node' => $file->filename));
  }
  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * @param $success
 * @param $results
 * @param $operations
 */
function generic_colour_batch_finished($success, $results, $operations){
  if ($success) {
    $message = count($results) .' images processed.';
    drupal_set_message($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}
