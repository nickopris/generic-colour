#Generic Colour

##About

Generic Colour is a Drupal 7 module using the [Colors-Of-Image library](https://github.com/humanmade/Colors-Of-Image)

It monitors the image uploads and calculates the most popular generic colours. These are made available as HEX values to be referenced in views or programatically.

'Color of Image differes from many palette extracters as it works off a white list color palette.'
 Below is the default palette:

![](https://dl.dropbox.com/u/238502/Captured/RUf54.png)


##Installation

Download all files and install them like any Drupal module. For more info, visit this link [Installing Modules and Themes](https://drupal.org/documentation/install/modules-themes)

##Usage

Allow roles to configure the module: visit the permissions page and enable _Configure Generic Colour_.
Turn on the monitorig by visiting the _admin/config/generic_colour_ page.
The module creates a new content type called *Generic colours*. New nodes will be added to this content type when a valid image has been uploaded and analyzed.

Testing the functionality - Upload an image anywhere on the site (except for the theme) and check for new nodes added under Content. These can be referenced in views or programatically.

##Thanks to
[https://github.com/humanmade/Colors-Of-Image](https://github.com/humanmade/Colors-Of-Image) - for the library that analyzes the images.

**@TODO**

- [x] Add permissions
- [x] Add dependencies
- [ ] Comment the code
- [x] Refactor the code
- [x] Process existing images
- [ ] Make fid field an actual file reference filed so we can use it in views
- [ ] Coding standards?